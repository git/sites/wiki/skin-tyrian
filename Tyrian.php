<?php
/**
 * Tyrian -- the new look of gentoo.org
 * MediaWiki implementation based on MonoBook nouveau.
 *
 * Copyright (C) 2014-2016 Alex Legler <a3li@gentoo.org>
 * Copyright (C) 2016-2023 Gentoo wiki project <wiki@gentoo.org>
 */

$wgExtensionCredits['skin'][] = array(
	'path' => __FILE__,
	'name' => 'Tyrian',
	'namemsg' => 'skinname-tyrian',
	'descriptionmsg' => 'tyrian-desc',
	'url' => 'https://www.gentoo.org/',
	'author' => array('Alex Legler'),
	'license-name' => 'GPLv2',
);

// Register files
$wgAutoloadClasses['SkinTyrian'] = __DIR__ . '/SkinTyrian.php';
$wgAutoloadClasses['TyrianTemplate'] = __DIR__ . '/TyrianTemplate.php';
$wgMessagesDirs['Tyrian'] = __DIR__ . '/i18n';

// Register skin
$wgValidSkinNames['tyrian'] = 'Tyrian';

// Register modules
$wgResourceModules['skins.tyrian.styles'] = array(
	'position' => 'top',
	'styles' => array(
		'main.css' => array('media' => 'screen'),
	),
	'remoteSkinPath' => 'Tyrian',
	'localBasePath' => __DIR__,
);

$wgHooks['OutputPageBeforeHTML'][] = 'injectMetaTags';

function injectMetaTags($out) {
	$out->addMeta('viewport', 'width=device-width, initial-scale=1.0');
	$out->addMeta('theme-color', '#54487a');
	return true;
}
