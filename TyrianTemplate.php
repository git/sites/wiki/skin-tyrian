<?php
/**
 * Tyrian -- the new look of gentoo.org
 * MediaWiki implementation based on MonoBook nouveau.
 *
 * Copyright (C) 2014-2016 Alex Legler <a3li@gentoo.org>
 * Copyright (C) 2016-2023 Gentoo wiki project <wiki@gentoo.org>
 */
class TyrianTemplate extends BaseTemplate {
	public function execute() {
		Wikimedia\AtEase\AtEase::suppressWarnings();

		$this->html( 'headelement' );

		$this->header();
		?>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="content" class="mw-body" role="main">
						<a id="top"></a>
						<?php if ( $this->data['sitenotice'] ) {
							echo '<div id="siteNotice">'; $this->html( 'sitenotice' ); echo '</div>';
							}	?>

						<h1 id="firstHeading" class="first-header" lang="<?php $this->data['pageLanguage'] = $this->getSkin()->getTitle()->getPageViewLanguage()->getHtmlCode(); $this->text( 'pageLanguage' );	?>">
							<span dir="auto"><?php $this->html( 'title' ); ?></span>
						</h1>

						<div id="bodyContent" class="mw-body-content">
							<div id="siteSub"><?php $this->msg( 'tagline' ) ?></div>
							<div id="contentSub"<?php	$this->html( 'userlangattributes' ); ?>>
								<?php $this->html( 'subtitle' )	?>
							</div>
							<?php	if ( $this->data['undelete'] ) {
									echo '<div id="contentSub2">'; $this->html( 'undelete' ); echo '</div>';
								}
								if ( $this->data['newtalk'] ) {
									echo '<div class="usermessage">'; $this->html( 'newtalk' ); echo '</div>';
								}	?>
					                <div id="jump-to-nav"></div>
					                <a class="mw-jump-link" href="#mw-head"><?php $this->msg('jumpto'); $this->msg( 'jumptonavigation' ); ?></a>
					                <a class="mw-jump-link" href="#searchInput"><?php $this->msg('jumpto'); $this->msg( 'jumptosearch' ); ?></a>

							<?php
							//<!-- start content -->
								$this->html( 'bodytext' );
								if ( $this->data['catlinks'] ) {
									$this->html( 'catlinks' );
								}
							//<!-- end content -->

								if ( $this->data['dataAfterContent'] ) {
									$this->html( 'dataAfterContent'	);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php
		$this->footer();
		$this->printTrail();
		echo Html::closeElement( 'body' );
		echo Html::closeElement( 'html' );
		Wikimedia\AtEase\AtEase::suppressWarnings(true);
	} // end of execute() method

	/*************************************************************************************************/

	private function header() {
	?>
	<div class="mw-jump sr-only">
		<?php	$this->msg( 'jumpto' ); ?>
		<a href="#top">content</a>
	</div>
	<header>
		<div class="site-title">
			<div class="container">
				<div class="row">
					<div class="site-title-buttons">
						<div class="btn-group btn-group-sm">
							<a href="https://get.gentoo.org/" role="button" class="btn get-gentoo"><span class="fa fa-fw fa-download"></span> <strong>Get Gentoo!</strong></a>
							<div class="btn-group btn-group-sm">
								<a class="btn gentoo-org-sites dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
									<span class="fa fa-fw fa-map-o"></span> <span class="hidden-xs">gentoo.org sites</span> <span class="caret"></span>
								</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="https://www.gentoo.org/" title="Gentoo's main website"><span class="fa fa-home fa-fw"></span> gentoo.org</a></li>
									<li><a href="https://wiki.gentoo.org/" title="Find and contribute documentation"><span class="fa fa-file-text-o fa-fw"></span> Wiki</a></li>
									<li><a href="https://bugs.gentoo.org/" title="Discover and report issues with Gentoo"><span class="fa fa-bug fa-fw"></span> Bugs</a></li>
									<li><a href="https://packages.gentoo.org/" title="Find software to install"><span class="fa fa-hdd-o fa-fw"></span> Packages</a></li>
									<li><a href="https://forums.gentoo.org/" title="Discuss with the community"><span class="fa fa-comments-o fa-fw"></span> Forums</a></li>
									<li class="divider"></li>
									<li><a href="https://planet.gentoo.org/" title="Find out what's going on in the dev community"><span class="fa fa-rss fa-fw"></span> Planet</a></li>
									<li><a href="https://archives.gentoo.org/" title="Read up on past discussions"><span class="fa fa-archive fa-fw"></span> Archives</a></li>
									<li><a href="https://devmanual.gentoo.org/" title="Read the development guide"><span class="fa fa-book fa-fw"></span> Devmanual</a></li>
									<li><a href="https://gitweb.gentoo.org/" title="Browse our source code in Gitweb"><span class="fa fa-code fa-fw"></span> Gitweb</a></li>
									<li class="divider"></li>
									<li><a href="https://infra-status.gentoo.org/" title="Get updates on the services provided by the Gentoo infra team"><span class="fa fa-server fa-fw"></span> Infra status</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="logo">
						<a href="/" title="Back to the homepage" class="site-logo">
							<img src="https://assets.gentoo.org/tyrian/site-logo.png"  alt="Gentoo Linux Logo" srcset="https://assets.gentoo.org/tyrian/site-logo.svg">
						</a>
						<span class="site-label">Wiki</span>
					</div>
				</div>
			</div>
		</div>
		<nav class="tyrian-navbar" role="navigation">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse navbar-main-collapse">
						<ul class="nav navbar-nav">
							<?php
							$this->renderPortals( $this->data['sidebar'] );
							?>
						</ul>
						<ul class="nav navbar-nav navbar-right hidden-xs">
							<?php
							$this->toolbox();
							$this->personaltools();
							?>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<?php $this->cactions(); ?>
	</header>
	<?php
	}

	private function footer() {
		$validFooterIcons = $this->getFooterIcons( "icononly" );
		$validFooterLinks = $this->getFooterLinks( "flat" ); // Additional footer links
		$currentYear = date('Y');
	?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-offset-2 col-md-7">
					<?php if ( count( $validFooterLinks ) > 0 ): ?>
						<div class="spacer"></div>
						<ul id="f-list">
							<?php	foreach ( $validFooterLinks as $aLink ) {
									if ($aLink === 'copyright') continue;
									echo "<li id=\"{$aLink}>\">"; $this->html( $aLink ); echo '</li>';
							} ?>
						</ul>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-md-3">
					<!-- No questions or comments, the Wiki has enough information on how to contact us. -->
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3 col-md-2">
					<ul class="footerlinks three-icons">
						<li><a href="https://twitter.com/gentoo" title="@Gentoo on Twitter"><span class="fa fa-twitter fa-fw"></span></a></li>
						<li><a href="https://www.facebook.com/gentoo.org" title="Gentoo on Facebook"><span class="fa fa-facebook fa-fw"></span></a></li>
						<li></li>
					</ul>
				</div>
				<div class="col-xs-9 col-md-9">
					<strong>&copy; 2001&ndash;<?=$currentYear ?> Gentoo Authors</strong><br />
					<small>
						Gentoo is a trademark of the Gentoo Foundation, Inc. and of Förderverein Gentoo e.V.
						The contents of this document, unless otherwise expressly stated, are licensed under the
						<a href="https://creativecommons.org/licenses/by-sa/4.0/" rel="license">CC-BY-SA-4.0</a> license.
						The <a href="https://www.gentoo.org/inside-gentoo/foundation/name-logo-guidelines.html">Gentoo Name and Logo Usage Guidelines</a> apply.
					</small>
				</div>
			</div>
		</div>
	</footer>
	<?php
		echo $footerEnd;
	}

	/**
	 * @param array $sidebar
	 */
	protected function renderPortals( array $sidebar ) {
		// These are already rendered elsewhere
		$sidebar['SEARCH'] = false;
		$sidebar['TOOLBOX'] = false;
		$sidebar['LANGUAGES'] = false;

		array_walk($sidebar, [$this, 'customBox'] );
	}

	private function searchBox() {
		?>
		<form action="<?php $this->text( 'wgScript' ) ?>" id="searchform" class="navbar-form navbar-right" role="search">
			<input type='hidden' name="title" value="<?php $this->text( 'searchtitle' ) ?>"/>

				<div class="input-group">
					<?php echo $this->makeSearchInput( [ "id" => "searchInput", "class" => "form-control", "placeholder" => $this->getMsg( 'search' )->escaped() ] ); ?>
					<div class="input-group-btn"><?php
						echo $this->makeSearchButton(
						"go",
						[ "id" => "searchGoButton", "class" => "searchButton btn btn-default" ]
					);

					echo $this->makeSearchButton(
						"fulltext",
						[ "id" => "mw-searchButton", "class" => "searchButton btn btn-default" ]
					);
				?></div>
				</div>
		</form>
	<?php
	}

	private function cactions() {
		$context_actions = [];
		$primary_actions = [];
		$secondary_actions = [];

		$assign_active = function(array &$actionTab) {
			if ( strpos( $actionTab['class'], 'selected' ) !== false ) {
				$actionTab['class'] .= ' active';
			}
		};

		foreach ( $this->data['content_actions'] as $key => $tab ) {
			// TODO: handling form_edit separately here is a hack, no idea how it works in Vector.
			if ( isset($tab['primary']) && $tab['primary'] == '1' || $key == 'form_edit' || $key == 'formedit' ) {
				if ( isset($tab['context']) ) {
					$context_actions[$key] = $tab;
					$assign_active($context_actions[$key]);
				} else {
					$primary_actions[$key] = $tab;
					$assign_active($primary_actions[$key]);
				}
			} else {
				$secondary_actions[$key] = $tab;
				$assign_active($secondary_actions[$key]);
			}
		}
		?>

		<nav class="navbar navbar-grey navbar-stick" id="wiki-actions" role="navigation">
			<div class="container"><div class="row">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gw-toolbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="gw-toolbar">
					<ul class="nav navbar-nav">
					<?php
						foreach ( $context_actions as $key => $tab ) {
							echo $this->makeListItem( $key, $tab );
						}
					?>
					</ul>
					<?php
						$this->searchBox();
					?>
					<ul class="nav navbar-nav navbar-right hidden-xs">
					<?php
						foreach ( $primary_actions as $key => $tab ) {
							echo $this->makeListItem( $key, $tab );
						}
					?><li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">more <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
					<?php
						foreach ( $secondary_actions as $key => $tab ) {
							echo $this->makeListItem( $key, $tab );
						}
					?>
							</ul>
						</li>
					</ul>
				</div>
			</div></div>
		</nav>
	<?php
	}

	/*************************************************************************************************/
	private function toolbox() {
		?>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i> <?php $this->msg( 'toolbox' ) ?> <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
				<?php
					$toolbox = $this->getSkin()->makeToolbox(
							$this->data['nav_urls'],
							$this->data['feeds']
						   );

					// Merge content that might be added to the toolbox section by hook
					if ( isset( $this->data['sidebar']['TOOLBOX'] ) ) {
						$toolbox = array_merge( $toolbox, $this->data['sidebar']['TOOLBOX'] ?? [] );
					}
					foreach ( $toolbox as $key => $tbitem) {
						echo $this->makeListItem( $key, $tbitem );
					}
				?>
			</ul>
		</li>
	<?php
	}

	/*************************************************************************************************/
	private function personaltools() {
		$personal_tools = $this->getPersonalTools();

		?>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				<span class="fa fa-user" aria-label="<?php $this->msg( 'personaltools' ) ?>"></span>
				<?php
					if (isset($personal_tools['userpage'])) {
						echo $personal_tools['userpage']['links'][0]['text'];
					} else {
						$this->msg( 'listfiles_user' );
					} ?>
				<span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<?php
					foreach ( $personal_tools as $key => $item ) {
						if ($key === 'notifications-alert') {
							$notifications_alert_tool = $item;
						} else if ($key === 'notifications-message') {
							$notifications_message_tool = $item;
						} else if ($key === 'notifications-notice') {
							$notifications_notice_tool = $item;
						} else {
							echo $this->makeListItem( $key, $item );
						}
					}
				?>
			</ul>
		</li>
		<?php

		if (isset($notifications_message_tool)) {
			echo $this->makeListItem('notifications-message', $notifications_message_tool);
		}

		if (isset($notifications_notice_tool)) {
			echo $this->makeListItem('notifications-notice', $notifications_notice_tool);
		}

		if (isset($notifications_alert_tool)) {
			echo $this->makeListItem('notifications-alert', $notifications_alert_tool);
		}
	}

	/*************************************************************************************************/
	private function languageBox() {
		if ( $this->data['language_urls'] !== false ):
			?>
			<div id="p-lang" class="portlet" role="navigation">
				<h3<?php $this->html( 'userlangattributes' ) ?>><?php $this->msg( 'otherlanguages' ) ?></h3>

				<div class="pBody">
					<ul>
						<?php foreach ( $this->data['language_urls'] as $key => $langlink ) {
								echo $this->makeListItem( $key, $langlink );
							}
						?>
					</ul>

					<?php $this->renderAfterPortlet( 'lang' ); ?>
				</div>
			</div>
		<?php
		endif;
	}

	/*************************************************************************************************/
	/**
	 * @param string $bar
	 * @param array|string $cont
	 */
	private function customBox( $cont, $bar ) {
		if ($cont === false) {
			return;
		}
		$msgObj = wfMessage( $bar );

		if ( $bar !== 'navigation' ) {
			$barQuoted = htmlspecialchars( $bar );
			$messageQuoted = htmlspecialchars( $msgObj->exists() ? $msgObj->text() : $bar );
			echo <<<EOT
			<li class="dropdown">
				<a href="/wiki/Gentoo_Wiki:Menu-{$barQuoted}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{$messageQuoted} <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
EOT;
		}

		if ( is_array ( $cont ) ) {
			foreach ( $cont as $key => $val ) {
				if ( $val['text'] === '---' ) {
					echo '<li role="presentation" class="divider"></li>';
				} elseif ( substr( $val['text'], 0, 7 ) === 'header:' ) {
					echo '<li role="presentation" class="dropdown-header">' . htmlspecialchars( substr( $val['text'], 7 ) ) . '</li>';
				} else {
					echo $this->makeListItem( $key, $val );
				}
			}
		} else {
			echo "<!-- This would have been a box, but it contains custom HTML which is not supported. -->";
		}

		if ( $bar !== 'navigation' ) {
			echo <<<EOT
				</ul>
			</li>
EOT;
		}
	}
}
