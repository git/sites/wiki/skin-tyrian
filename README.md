Tyrian skin for MediaWiki
======

Tyrian – The new look of gentoo.org.

This skin's main repository is hosted [here](https://gitweb.gentoo.org/sites/wiki/skin-tyrian.git).

Find the Tyrian theme source [here](https://gitweb.gentoo.org/sites/tyrian-theme.git/).

This skin overwrites *some* CSS found in the Tyrian theme in a way that is appropiate for rendering on MediaWiki. See the main.css file for details.

It may be possible to test experimental changes to CCS classes by editing the wiki's [Common.css](https://wiki.gentoo.org/wiki/MediaWiki:Common.css) page. Long term, wiki-specific changes should be promoted into this file and removed from Common.css upon succesful testing.
