<?php
/**
 * Tyrian -- the new look of gentoo.org
 * MediaWiki implementation based on MonoBook nouveau.
 *
 * Copyright (C) 2014-2016 Alex Legler <a3li@gentoo.org>
 * Copyright (C) 2016-2023 Gentoo wiki project <wiki@gentoo.org>
 */

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @ingroup Skins
 */
class SkinTyrian extends SkinTemplate {
	public $skinname  = 'tyrian';
	public $stylename = 'Tyrian';
	public $template  = 'TyrianTemplate';

	private $output;

	const CDN_URL = 'https://assets.gentoo.org/tyrian/';

	public function setupTyrianSkinUserCss(OutputPage $out) {
		$this->output = $out;

		$out->addStyle(SkinTyrian::CDN_URL . 'bootstrap.min.css');
		$out->addStyle(SkinTyrian::CDN_URL . 'tyrian.min.css');

		$out->addModuleStyles([
			'mediawiki.skinning.interface',
			'mediawiki.skinning.content.externallinks',
			'skins.tyrian.styles',
			'skins.tyrian.icons'
		]);
	}
	public static function injectMetaTags($out) {
		$out->addMeta('viewport', 'width=device-width, initial-scale=1.0');
		$out->addMeta('theme-color', '#54487a');
		return true;
	}

	public function initPage( OutputPage $out ) {
		parent::initPage( $out );
		$this->setupTyrianSkinUserCss( $out );
		$cdnURL = self::CDN_URL . 'bootstrap.min.js';
		$script = <<<EOS
function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(function() { mw.loader.load( '$cdnURL'); });
EOS;
		$out->addInlineScript($script);
	}
}
